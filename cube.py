class Cube:
    def __init__(self, width, length, height) -> None:
        if width < 0 or length < 0 or height < 0:
            raise ValueError

        self.width = width
        self.length = length
        self.height = height

    def get_volume(self):
        return self.length * self.width * self.height
