import unittest
from cube import Cube


class TestCube(unittest.TestCase):

    def test_dimensions(self):
        cube = Cube(1, 2, 3)
        self.assertEqual(cube.width, 1)
        self.assertEqual(cube.length, 2)
        self.assertEqual(cube.height, 3)

    def test_dimensions_non_positive(self):
        self.assertRaises(ValueError, Cube, -1, 0, -2)

    def test_dimensions_string(self):
        self.assertRaises(TypeError, Cube, "1", "2", "3")
        self.assertRaises(TypeError, Cube, 1, "2", "3")
        self.assertRaises(TypeError, Cube, "1", 2, "3")
        self.assertRaises(TypeError, Cube, "1", "2", 3)
        self.assertRaises(TypeError, Cube, 1, 2, "3")
        self.assertRaises(TypeError, Cube, 1, "2", 3)

    def test_volume_int(self):
        cube = Cube(2, 3, 6)
        self.assertEqual(cube.get_volume(), 36)

    def test_volume_float(self):
        cube = Cube(2.4, 3.5, 6.7)
        self.assertEqual(cube.get_volume(), 56.28)

    def test_volume_unit(self):
        cube = Cube(1, 1, 1)
        self.assertEqual(cube.get_volume(), 1)


if __name__ == "__main__":
    unittest.main()
